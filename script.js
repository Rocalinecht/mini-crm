$.getJSON('data/crm.json', function (data) {
    console.log(data)

    // MICHEL DRUCKER
    $("#michel").click(function () {
        //info generale
        var firts_name = data.customers[0].firts_name
        var last_name = data.customers[0].last_name
        var picture = data.customers[0].picture
        var birthday = data.customers[0].birthday
        var email = data.customers[0].email
        var phone = data.customers[0].phone
        var company = data.customers[0].company
        var title = data.customers[0].title
        var description = data.customers[0].desciption
        //notes
        var subject = data.customers[0].notes[0].subject
        var note = data.customers[0].notes[0].note
        var related_to = data.customers[0].notes[0].related_to
        //tags
        var tags_journaliste = data.customers[0].tags[0]
        var tags_france = data.customers[0].tags[1]
        var tags_tv = data.customers[0].tags[2]
        var tags_france2 = data.customers[0].tags[3]
        var tags_europe1 = data.customers[0].tags[4]
        //tasks
        var tasks_tak = data.customers[0].tasks[0].task
        var tasks_category = data.customers[0].tasks[0].category
        var tasks_date = data.customers[0].tasks[0].date
        var tasks_owner = data.customers[0].tasks[0].owner
        var tasks_priority = data.customers[0].tasks[0].priority
        var tasks_status = data.customers[0].tasks[0].status
        var tasks_relatedto = data.customers[0].tasks[0].related_to
        var tasks_relateaddeal = data.customers[0].tasks[0].related_deal

        /*-------MUSTACHE : MISE EN PLACE DANS LE HTML----*/
        var dataBloc = {
            firts_name: firts_name, last_name: last_name, picture: picture, description: description, birthday: birthday, company: company
            , note: note, subject: subject, related_to: related_to, tags_journaliste: tags_journaliste, tags_france: tags_france, tags_tv: tags_tv,
            tags_france2: tags_france2, tags_europe1: tags_europe1, email: email, phone: phone, tasks_tak: tasks_tak, tasks_category: tasks_category,
            tasks_date: tasks_date, tasks_owner: tasks_owner, tasks_priority: tasks_priority, tasks_status: tasks_status, tasks_relatedto: tasks_relatedto,
            tasks_relateaddeal: tasks_relateaddeal
        }

        var templateBloc = "<div class='info'>\
    <img src='{{picture}}'></img>\
    <div class='nom'>\
    <h1>{{firts_name }} {{last_name }}</h1>\
    <p classe='naissance'>{{birthday}}</p>\
    <div class='contact'><p classe='email'>{{email}}</p><p classe='phone'>{{phone}}</p><p classe='company'>{{company}}</p></div>\
    <p classe='presentation'>{{description}}</p>\
    </div>\
    </div>\
    <div class='sectionBloc'>\
    <div class='notes'><h2>Notes</h2>\
    <p>{{subject}}</p>\
    <p>{{note}}</p>\
    <p>{{related_to}}</p>\
    </div><div class='tacks'><h2>Tacks</h2>\
    <ul><li>{{tasks_tak}}</li>\
    <li>{{tasks_category}}</li>\
    <li>{{tasks_date}}</li>\
    <li>{{tasks_owner}}</li>\
    <li>{{tasks_priority}}</li>\
    <li>{{tasks_status}}</li>\
    <li>{{tasks_relatedto}}</li>\
    <li>{{tasks_relateaddeal}}</li></ul>\
    </div></div>\
    <div class='tags'><h2>Tags</h2>\
    <div class='infoTags'><p>{{tags_journaliste}}</p>\
    <p>{{tags_france}}</p>\
    <p>{{tags_tv}}</p>\
    <p>{{tags_france2}}</p>\
    <p>{{tags_europe1 }}</p></div>\
    </div>";
        var output = Mustache.render(templateBloc, dataBloc)
        $('#app').append(output);
    })

    // FREDERIC
    $("#fred").click(function () {
        var firts_name1 = data.customers[1].firts_name
        var last_name1 = data.customers[1].last_name
        var picture1 = data.customers[1].picture
        var birthday1 = data.customers[1].birthday
        var email1 = data.customers[1].email
        var phone1 = data.customers[1].phone
        var company1 = data.customers[1].company
        var title1 = data.customers[1].title
        var description1 = data.customers[1].description
        //notes
        var subject = data.customers[1].notes[0].subject
        var note = data.customers[1].notes[0].note
        var related_to = data.customers[1].notes[0].related_to
        //tags
        var tags2 = data.customers[1].tags[0]
        //tasks
        var tasks_tak = data.customers[1].tasks[0].task
        var tasks_category = data.customers[1].tasks[0].category
        var tasks_date = data.customers[1].tasks[0].date
        var tasks_owner = data.customers[1].tasks[0].owner
        var tasks_priority = data.customers[1].tasks[0].priority
        var tasks_status = data.customers[1].tasks[0].status
        var tasks_relatedto = data.customers[1].tasks[0].related_to
        var tasks_relateaddeal = data.customers[1].tasks[0].related_deal

        /*-------MUSTACHE : MISE EN PLACE DANS LE HTML----*/
        var dataBloc = {
            firts_name: firts_name1, last_name: last_name1, picture: picture1, description:description1, birthday: birthday1, company: company1
            , note: note, subject: subject, related_to: related_to, tags: tags2, email: email1, phone: phone1, tasks_tak: tasks_tak, tasks_category: tasks_category,
            tasks_date: tasks_date, tasks_owner: tasks_owner, tasks_priority: tasks_priority, tasks_status: tasks_status, tasks_relatedto: tasks_relatedto,
            tasks_relateaddeal: tasks_relateaddeal
        }

        var templateBloc = "<div class='info'>\
     <img src='{{picture}}'></img>\
     <div class='nom'>\
     <h1>{{firts_name }} {{last_name }}</h1>\
     <p classe='naissance'>{{birthday}}</p>\
     <div class='contact'><p classe='email'>{{email}}</p><p classe='phone'>{{phone}}</p><p classe='company'>{{company}}</p></div>\
     <p classe='presentation'>{{description}}</p>\
     </div>\
     </div>\
     <div class='sectionBloc'>\
     <div class='notes'><h2>Notes</h2>\
     <p>{{subject}}</p>\
     <p>{{note}}</p>\
     <p>{{related_to}}</p>\
     </div><div class='tacks'><h2>Tacks</h2>\
     <ul><li>{{tasks_tak}}</li>\
     <li>{{tasks_category}}</li>\
     <li>{{tasks_date}}</li>\
     <li>{{tasks_owner}}</li>\
     <li>{{tasks_priority}}</li>\
     <li>{{tasks_status}}</li>\
     <li>{{tasks_relatedto}}</li>\
     <li>{{tasks_relateaddeal}}</li></ul>\
     </div></div>\
     <div class='tags'><h2>Tags</h2>\
     <div class='infoTags'><p>{{tags}}</p>\
    </div>\
     </div>";
        var output = Mustache.render(templateBloc, dataBloc)
        $('#app').append(output);

    })

    //PATRICk   

    $("#patrick").click(function () {
        var firts_name2 = data.customers[2].firts_name
        var last_name2 = data.customers[2].last_name
        var picture2 = data.customers[2].picture
        var birthday2 = data.customers[2].birthday
        var email2 = data.customers[2].email
        var phone2 = data.customers[2].phone
        var company2 = data.customers[2].company
        var title2 = data.customers[2].title
        var description2 = data.customers[2].description
        //notes
        var note2 = data.customers[2].notes[0]
        //tags
        var tags_journaliste2 = data.customers[2].tags[0]
        var tags_france2deux = data.customers[2].tags[1]
        var tags_tv2 = data.customers[2].tags[2]
        var tags_france22 = data.customers[2].tags[3]
        var tags_europe12 = data.customers[2].tags[4]
        //tasks
        var tasks_tak2 = data.customers[2].tasks[0]
        
        /*-------MUSTACHE : MISE EN PLACE DANS LE HTML----*/
        var dataBloc = {
            firts_name: firts_name2, last_name: last_name2, picture: picture2, description: description2, birthday: birthday2, company: company2
            , note: note2, tags_journaliste: tags_journaliste2, tags_france: tags_france2deux, tags_tv: tags_tv2,
            tags_france2: tags_france22, tags_europe1: tags_europe12, email: email2, phone: phone2, tasks_tak: tasks_tak2,
        }

        var templateBloc = "<div class='info'>\
     <img src='{{picture}}'></img>\
     <div class='nom'>\
     <h1>{{firts_name }} {{last_name }}</h1>\
     <p classe='naissance'>{{birthday}}</p>\
     <div class='contact'><p classe='email'>{{email}}</p><p classe='phone'>{{phone}}</p><p classe='company'>{{company}}</p></div>\
     <p classe='presentation'>{{description}}</p>\
     </div>\
     </div>\
     <div class='sectionBloc'>\
     <div class='notes'><h2>Notes</h2>\
     <p>{{subject}}</p>\
     <p>{{note}}</p>\
     <p>{{related_to}}</p>\
     </div><div class='tacks'><h2>Tacks</h2>\
     <ul><li>{{tasks_tak}}</li>\
     <li>{{tasks_category}}</li>\
     <li>{{tasks_date}}</li>\
     <li>{{tasks_owner}}</li>\
     <li>{{tasks_priority}}</li>\
     <li>{{tasks_status}}</li>\
     <li>{{tasks_relatedto}}</li>\
     <li>{{tasks_relateaddeal}}</li></ul>\
     </div></div>\
     <div class='tags'><h2>Tags</h2>\
     <div class='infoTags'><p>{{tags_journaliste}}</p>\
     <p>{{tags_france}}</p>\
     <p>{{tags_tv}}</p>\
     <p>{{tags_france2}}</p>\
     <p>{{tags_europe1 }}</p></div>\
     </div>";

        var output = Mustache.render(templateBloc, dataBloc)
        $('#app').append(output);


    })
});








