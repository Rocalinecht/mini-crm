// function loadUser() {
//     var template = $('#template').html();
//     Mustache.parse(template);   // optional, speeds up future uses
//     var rendered = Mustache.render(template, {name: "Luke"});
//     $('#target').html(rendered);
//   }

function loadUser() {
    $.get('template.mst', function(template) {
      var rendered = Mustache.render(template, {name: "Luke"});
      $('#target').html(rendered);
    });
}